import ArisGraphLibrary as aris
import json
import os
import random
from multiprocessing import Process, JoinableQueue, Pool
from threading import Thread
import time
import numpy as np

CORPUS_PATH = "<path to CSV corpus>"

global runtime_results
runtime_results = []

def compute_vector_similarity(a, b):

    a = np.array(a)
    b = np.array(b)
    return round(1/(abs(np.linalg.norm(a-b))+1), 2)*100

# End of compute_vector_similarity.

def load_json_data():

    with open('content_vectors.json') as f:
        data = json.load(f)
        return data

# End of load_json_data.

def save_json_data(data):

    with open('content_vectors.json', 'w') as outfile:
        json.dump(data, outfile, sort_keys=True, indent=4)

# End of save_json_data.

def check(G1, G2):
    GM = aris.GraphMatcher(G1, G2)
    GM.vf2_match()
    return GM.similarity_score

def retrieve(data, G):

    MAX_ATTEMPT = 5
    MAX_TIMEOUT = 5

    content_vector_dictionary = data['content_vector_dictionary']
    content_vector_results = content_vector_dictionary["-".join([str(x) for x in G.content_vector])]
    print(f"found {len(content_vector_results)} potential match")
    
    greatest_match = aris.ConceptualGraph(CORPUS_PATH + content_vector_results[0]['file'])
    
    # GM = aris.ArisGraphMatcher(G, greatest_match)
    # GM.vf2_match()
    result = 0
    for i in range(MAX_ATTEMPT):
        try:
            with Pool(processes=1) as pool:
                results = pool.apply_async(check, (G, greatest_match))
                result = results.get(MAX_TIMEOUT)
                pool.terminate()
                break
        except:
            print(f"attempt {i+1} timed out!")
            pass

    greatest_score = result

    matched = []
    for entry in content_vector_results:
        result = 0
        # entry_graph = aris.ConceptualGraph(CORPUS_PATH + entry['file'])
        # GM = aris.ArisGraphMatcher(G, entry_graph)
        # GM.vf2_match()
        entry_graph = aris.ConceptualGraph(CORPUS_PATH + entry['file'])
        for i in range(MAX_ATTEMPT):
            try:
                with Pool(processes=1) as pool:
                    results = pool.apply_async(check, (G, entry_graph))
                    result = results.get(MAX_TIMEOUT)
                    pool.terminate()
                    del pool # not tested yet.
                    break
            except:
                print(f"attempt {i+1} timed out!")
                pass
        
        if result == 100:
            matched.append(entry_graph.graph['Graphid'])
    
    return matched

# End of retrieve.

def generate_content_vector_dictionary():
    data = load_json_data()
    content_vector_dictionary = data['content_vector_dictionary']

    start = time.time()
    for file_name in os.listdir(CORPUS_PATH):
        try:
            if os.path.getsize(CORPUS_PATH + file_name) != 0: # if file not empty
                G = aris.ConceptualGraph(CORPUS_PATH + file_name)
                key = "-".join(str(x) for x in G.content_vector)

                if key in content_vector_dictionary:
                    content_vector_dictionary[key].append({"file": G.graph['Graphid'], "method": G.method_name})
                else:
                    content_vector_dictionary[key] = []
                    content_vector_dictionary[key].append({"file": G.graph['Graphid'], "method": G.method_name})
        except:
            pass

    with open('data.json', 'w') as outfile:
        json.dump(data, outfile, sort_keys=True, indent=4)

    # This is to get the runtime.
    # print(time.time() - start)

# End of generate_content_vector_dictionary

def test_retrieval():

    data = load_json_data()
    content_vector_dictionary = data['content_vector_dictionary']

    allowed = [k for k,v in content_vector_dictionary.items() if len(v) < 500]
    all_methods = []
    for metric in allowed:
        all_methods += content_vector_dictionary[metric]

    # Shuffle the list 3 times to make sure we have something really random
    random.shuffle(all_methods)
    random.shuffle(all_methods)
    random.shuffle(all_methods)

    for method in all_methods[:1000]:
        G = aris.ConceptualGraph(CORPUS_PATH + method['file'])
        start = time.time()
        GM = aris.GraphMatcher(G, G)
        GM.vf2_match()
        runtime_results.append({"file": method["file"], "nodes": len(list(G.nodes())), "runtime": time.time() - start})

    with open("retrieval_results.json") as f:
        all_methods = [{'file': x['method']} for x in json.load(f)]

    results = []

    success = 0
    failures = 0

    for i,method in enumerate(all_methods[:1000]):
        result = {
            "method": method['file'],
            "nodes": 0,
            "retrieved": False,
            "retrieval runtime": 0,
            "graph runtime": 0,
            "total similar": 0,
            "group size": 0
        }
        
        start = time.time()
        G = aris.ConceptualGraph(CORPUS_PATH + method['file'])
        result["graph runtime"] = time.time() - start

        result["nodes"] = len(list(G.nodes()))

        start = time.time()
        matched = retrieve(data, G)
        result["retrieval runtime"] = time.time() - start
        result["total similar"] = len(matched)

        if method['file'] in matched: 
            print(f"method found itself :) In total found {len(matched)} other similar graphs")
            result["retrieved"] = True
            success += 1
        else:
            print(f"method did not found itself :( In total found {len(matched)} other similar graphs")
            failures += 1

        print(f"{i+1}/1000 methods checked | success: {success}, failures: {failures}")
        results.append(result)

    with open('results.json', 'w') as outfile:
        json.dump(results, outfile, sort_keys=True, indent=4)
        
# End of test_retrieval.