import csv
import json
import networkx as nx
from networkx.algorithms import isomorphism
import os
import re
import sys
import webbrowser

class GraphMatcher(isomorphism.DiGraphMatcher):

    def candidate_pairs_iter(self):

        """Iterator over candidate pairs of nodes in G1 and G2."""

        # All computations are done using the current state!

        G1_nodes = self.G1_nodes
        G2_nodes = self.G2_nodes

        # First we compute the out-terminal sets.
        T1_out = [node for node in G1_nodes if (node in self.out_1) and (node not in self.core_1)]
        T2_out = [node for node in G2_nodes if (node in self.out_2) and (node not in self.core_2)]

        # If T1_out and T2_out are both nonempty.
        # P(s) = T1_out x {min T2_out}

        if self.first_pair:

            self.first_pair = False
            yield self.G1.get_root(), self.G2.get_root

        else:

            if T1_out and T2_out:
                node_2 = min(T2_out)
                for node_1 in T1_out:
                    yield node_1, node_2

            # If T1_out and T2_out were both empty....
            # We compute the in-terminal sets.

            # elif not (T1_out or T2_out):   # as suggested by [2], incorrect
            else:                            # as suggested by [1], correct
                T1_in = [node for node in G1_nodes if (node in self.in_1) and (node not in self.core_1)]
                T2_in = [node for node in G2_nodes if (node in self.in_2) and (node not in self.core_2)]

                # If T1_in and T2_in are both nonempty.
                # P(s) = T1_out x {min T2_out}
                if T1_in and T2_in:
                    node_2 = min(T2_in)
                    for node_1 in T1_in:
                        yield node_1, node_2

                # If all terminal sets are empty...
                # P(s) = (N_1 - M_1) x {min (N_2 - M_2)}

                # elif not (T1_in or T2_in):   # as suggested by  [2], incorrect
                else:                          # as inferred from [1], correct
                    node_2 = min(G2_nodes - set(self.core_2))
                    for node_1 in G1_nodes:
                        if node_1 not in self.core_1:
                            yield node_1, node_2

    # End of candidate_pairs_iter.

    def compute_concept_similarty(self, verbose=False):

        similarity_score = 0
        if (len(self.mapping) == 0):
            if verbose:
                print("No Mapping!")
                print(f"0.0% similarity")
            return 0
        else:
            s_graph_map = {node: attr['label'] for node, attr in self.G1.nodes(data=True)}
            t_graph_map = {node: attr['label'] for node, attr in self.G2.nodes(data=True)}
            
            for s_node, t_node in self.mapping.items():
                if s_graph_map[s_node].split(":")[0] == t_graph_map[t_node].split(":")[0]:
                    if verbose:
                        print(f" +1 {s_graph_map[s_node]} <-> {t_graph_map[t_node]}")
                    similarity_score += 1
                else:
                    if verbose:
                        print(f" +0 {s_graph_map[s_node]} \/ {t_graph_map[t_node]}")
            
            for node in self.G1.nodes():
                if node not in self.mapping:
                    if verbose:
                        print(f" +0 No Mapping for {s_graph_map[node]}")

            mapping = {v: k for k, v in self.mapping.items()}
            for node in self.G2.nodes():
                if node not in mapping:
                    if verbose:
                        print(f" +0 No Mapping for {t_graph_map[node]}--")
            
            max_nodes = max(len(list(self.G1.nodes())), len(list(self.G2.nodes())))
            if verbose:
                print(f"{similarity_score} node(s) matched out of {max_nodes}")

            score = round((similarity_score / max_nodes), 2)*100
            if verbose:
                print(f"{score}% simlarity")
            self.similarity_score = score
            return score

    # End of compute_concept_similarity.

    def compute_relation_similarity(self, verbose=False):

        score = 0
        similarity_score = 0
        for a, b in self.mapping.items():
            G1_edges = sorted([attr['label'] for x, y, attr in self.G1.edges(data=True) if x is a])
            G2_edges = sorted([attr['label'] for x, y, attr in self.G2.edges(data=True) if x is b])
            if verbose:
                print(G1_edges)
                print(G2_edges)
            score += len(list(filter(lambda x: x == True, [x == y for x,y in zip(G1_edges, G2_edges)])))

            if verbose:
                print(f"#################### {score} / {len(list(self.G1.edges()))}")
            similarity_score = (score / len(list(self.G1.edges()))) * 100
        if verbose:
            print(f"Score = {similarity_score}")
        return similarity_score

    # end of calculate_relation_comparison.

    def compute_similarity(self, verbose=False):
        concept_score = self.compute_concept_similarty()
        relation_score = self.compute_relation_similarity()

        concept_weight = 0.5
        relation_weight = 0.5

        similarity_score = (concept_weight * concept_score) + (relation_weight * relation_score)
        self.similarity_score = similarity_score
        return similarity_score

    # End of compute_cimilarity.

    def get_node_attr(self, G, name):

        node, attr = [(node, attr) for node, attr in G.nodes(data=True) if node == name][0]
        return attr

    # End of get_node_attr.

    def semantic_feasibility(self, G1_node, G2_node):

        G1_node_label = self.get_node_attr(self.G1, G1_node)['label']
        G2_node_label = self.get_node_attr(self.G2, G2_node)['label']

        # Temporay fix for Java and C# Conceptual Graph compatibility.
        if "Block: Root" in G1_node_label or "Class:" in G1_node_label:
            if "Block: Root" in G2_node_label or "Class:" in G2_node_label:
                return True
        else:
            return G1_node_label.split(":")[0] == G2_node_label.split(":")[0] 

    # End of semantic_feasability.

    def vf2_match(self, verbose=False):

        self.first_pair = False
        self.similarity_score = 0
        if len(list(self.G1.nodes())) < 150 or len(list(self.G2.nodes())) < 150:
            self.subgraph_is_isomorphic()
            return self.compute_similarity(verbose)
        else:
            print("Graphs too large for matching")
            return self.similarity_score

    # End of match.

# End of GraphMatcher class.


class ConceptualGraph(nx.DiGraph):

    def __init__(self, file_path=""):
        
        super(ConceptualGraph, self).__init__()

        if file_path != "":
            self.load_from_csv(file_path)
            self.compute_content_vector()

    # End of __init__.

    def compute_max_depth(self):        
        root = self.get_root()

        depth_limit = 1
        depth_nodes = {}
        graph_size = len(self.nodes())

        while depth_limit < graph_size:
            temp_nodes = nx.dfs_successors(self, root, depth_limit)

            if temp_nodes == depth_nodes:
                break
            else:
                depth_nodes = temp_nodes
                depth_limit += 1

        return depth_limit

    # End of compute_max_depth

    def compute_content_vector(self, verbose=False):

        number_of_relations = len(self.edges())
        number_of_concepts = len(self.nodes())
        largest_connected_component = max([len(list(self.neighbors(x))) for x in self.nodes()])
        
        try:
            average_shortest_path = int(nx.average_shortest_path_length(self)*100)
        except:
            average_shortest_path = 0

        max_depth = self.compute_max_depth()

        Edges = self.edges(data=True)
        number_of_conditions = len([x for x, y, attr in Edges if "Condition" in attr['label']])
        number_of_contains = len([x for x, y, attr in Edges if "Contains" in attr['label']])
        number_of_parameters = len([x for x, y, attr in Edges if "Parameter" in attr['label']])
        number_of_depends = len([x for x, y, attr in Edges if "Depends" in attr['label']])
        number_of_returns = len([x for x, y, attr in Edges if "Returns" in attr['label']])

        content_vector = [
            number_of_relations,
            number_of_concepts,
            largest_connected_component,
            average_shortest_path,
            max_depth,
            number_of_conditions,
            number_of_contains,
            number_of_parameters,
            number_of_depends,
            number_of_returns
        ]

        if verbose:
            print(f"number of relations = {number_of_relations}")
            print(f"number of concepts = {number_of_concepts}")
            print(f"largest connected component = {largest_connected_component}")
            print(f"average shortest path length = {average_shortest_path}")
            print(f"number of conditions = {number_of_conditions}")
            print(f"number of contains = {number_of_contains}")
            print(f"number of parameters = {number_of_parameters}")
            print(f"number of depends = {number_of_depends}")
            print(f"number of returns = {number_of_returns}")

        self.content_vector = content_vector
        return content_vector

    # end of compute_content_vector.

    def get_root(self):
        return [node for node, attr in self.nodes(data=True) if "Block: Root" in attr['label'] or "Class:" in attr['label']][0]

    def load_from_csv(self, file_path):

        with open(file_path, 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            file_name = re.split("/|\\\\", file_path)[-1]
            # print("Data file: ", file_name)
            self.graph['Graphid'] = file_name
            try:
                for row in csvreader:
                    method_name, subject, obj, verb, source_id, target_id = row
                    self.add_node(source_id, label=subject)
                    self.add_node(target_id, label=verb)
                    self.add_edge(source_id, target_id, label=obj)
                self.method_name = method_name
            except:
                raise ValueError(f"Badly formattedd CSV, file: {file_name}")

    # End of load_from_csv.

    def show(self):

        data = {
            'nodes': [],
            'edges': []
        }

        # D3js refers to nodes by their index value,
        # Therefore a mapping between the node and its index
        # is required to transfer the edges correctly.
        node_map = {k: v for v, k in enumerate(self.nodes())}

        for node_id, attr in self.nodes(data=True):
            data['nodes'].append({'nodeID': node_id, 'label': attr['label']})

        for source, target, attr in self.edges(data=True):
            data['edges'].append(
                {
                    'source': node_map[source],
                    'target': node_map[target],
                    'label': attr['label']
                }
            )

        with open('data.json', 'w') as outfile:
            json.dump(data, outfile, indent=4, sort_keys=True)

        webbrowser.get("open -a /Applications/Firefox.app %s").open('file://' + os.path.realpath('graph.html'))

    # End of show.

# End of ConceptualGraph class.