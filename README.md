# Aris Code Retrieval
This project aims to retrieve source code that are similar or itself from within the a corpus of source code.

For the time being this project mainly works with C# source code that was converted in a CSV format using the [C#2csv](https://gitlab.com/aris_project/CSharp2csv) project.

This project uses the Networkx[link] library to create graphs and to use it's VF2 graph matching algorithm implementation.

## Installation
You will need Python 3.x and the following libraries:
- [Numpy](http://www.numpy.org/)
- [Networkx](https://networkx.github.io/)

Then clone the git repository, then you can simply run the script using an IDE such as [PyCharm](https://www.jetbrains.com/pycharm/download/) or in your command prompt/terminal.

`ArisGraphLibrary.py` has to be in the same directory as your own Python script to import it.

You will also need to have [Firefox](https://www.mozilla.org/en-US/firefox/new/) installed if you wish to visualise the graphs.

## How to Use

If you wish to replice our results using our corpus of C# methods you can download the [CSV corpus](https://mega.nz/#!lzIgBQAQ!soHVncZNsP2WVwzfId7PgCmMEUtyJ8OBXFcnalwTw2E) which is all the C# methods converted in CSV format and ready to be loaded in.

Otherwise you can generate a corpus of your own using the [C#2csv](https://gitlab.com/aris_project/CSharp2csv) project, which was used to generate all of our CSVs.

Then you have to generate your content vector dictionary by using the `generate_content_vector_dictionary()` method, a contents_vector.json will be created, or you can download the [contents_vector.json](https://mega.nz/#!AyREgIjQ!cit9QMjGU2fkVUrbI6JjuXGXPsD8hfJGpj-tD-AixRo) that was created from the [CSV corpus](https://mega.nz/#!lzIgBQAQ!soHVncZNsP2WVwzfId7PgCmMEUtyJ8OBXFcnalwTw2E).

Create a conceptual graph:
```Python
import ArisGraphLibrary as aris

G = aris.ConceptualGraph("<path to CSV file>")
```

Create a graph matcher to compare two graphs:
```Python
import ArisGraphLibrary as aris

G1 = aris.ConceptualGraph("<path to CSV file 1>")
G2 = aris.ConceptualGraph("<path to CSV file 2>")

GM = aris.GraphMatcher(G1, G2)
print(GM.vf2_match())
```
*Note:* If you wish to compare two graphs of different sizes were G2 is a subgraph of G1 then you must insert G1 first and then G2 as shown in the example above, otherwise a score of 0% will be returned even if G2 is a subgraph of G1.

## Conceptual Graph Class
The Conceptual Graph represents the ...

#### Graph Visualisation
You can use the `show()` method to see the graph, you need [Firefox](https://www.mozilla.org/en-US/firefox/new/) installed has it allows XSS.
The method will generate a JSON file that represents the graph nodes and edges, then aa Firefox page will open and run JavaScript using [D3JS](https://d3js.org/) that will generate the graphics, it works in a similar fashion has [Neo4j](https://neo4j.com/).

*Example:*
```Python
import ArisGraphLibrary as aris

G = aris.ConceptualGraph("<path to CSV file>")
G.show()
```

*Here is a sample:*
![sample graph](./documentation/images/for_sigma.png)

## Results
In `retrieval.py` there is a method called `test_retrieval()` this will test the accuracy of the retrieval system and will create `results.json`. A copy of our own results can be downloaded here [results.json](https://mega.nz/#!5jACRaQD!Yl5kTUyH7pKE6XsXxCE9hLb8kSLgoQ2VRlNaDY3ZZfo).

## Testing
Th VF2 graph matching algorithm was used to test a set of 50 C# methods, source code can be downloaded [here](https://mega.nz/#!FqIFHaBD!VgZyHB3RVAWI6wOn_T5SBSWHcDDYFr74tvbTtQ-ZrUM) and their CSVs [here](https://mega.nz/#!c3RT3QDA!2sNXW7eLHmC4XALLPIc_YcO9zl9RQbOGDwAXJn3cWYE).
Those 50 methods have the original source code (Test Suite 1 / Isomorphic) and 3 levels:
- level 1 (Test Suite 2 / Near Isomorphic)
- level 2 (Test Suite 3 / Homomorphic)
- level 3 (Test Suite 4 / Dissimilar)

## Issues
The VF2 implementation in [Networkx](https://networkx.github.io/) is a tends to have difficulties at random with graphs roughly exceeding a size of 150 nodes. You may get run VF2 and it will run forever without ever returning an answer, but restarting it a few times may give you a results in an instant. Also very large graphs containing thouands of nodes may never return an answer. 

This project has code implemented to run the matching in a process and timeout after 5 seconds, and try the graph matching 5 times before abandonning the operation.